
| ⚠️ Warning                               | 
|------------------------------------------|
| This repository has been unmaintained for a few month. Please consider using another tool like https://gitlab.com/gaia-x/lab/compliance/gx-registry |


# Trusted Registry

The Trusted Registry allow to :
* Retrieve trust anchors : the trusted DID (that identify trusted issuers) and trusted x509 certificates (that can be user by trusted issuers if they don't have DID).
* Validate a VC / VP against trusted schemas and trusted issuers.


In the well know "Triangle of trust" of SSI word, the Trusted Registry is the component used by **Verifier** to **discover who he can trust**.

```mermaid
flowchart LR

i(Issuer)
h(Holder)
v(Verifier)
i--"issue\n(credentials)"-->h--"present\n(credentials)"-->v--trust-->i
```

* A trusted **issuer** issues cryptographically verifiable credentials.
* The **holder** (user) stores these credentials (which contain all relevant information and attributes), and presents them to a verifier (e.g. a service provider) to prove his identity and attributes (rights).
* The **verifier** can check the authenticity of the presented credentials by verifying its cryptographic signature using the trusted issuer's public key.

## How to use it

> ## A live instance of the Trusted Registry is available at https://trusted-registry.abc-federation.gaia-x.community/swagger-ui/


You can either :

 Build the source after checkout the source repository with the command :

```bash
cargo build --release
```

Or download precompiled builds for your OS / architecture from download section.

Then launch to get launch options :

```bash
A simple trusted registry

Usage: trusted-registry [OPTIONS] <COMMAND>

Commands:
  serve            Launch Trusted Registry server as read only mode
  add-pem-cert     Add Base 64 encoded PEM certificate in the registry
  add-der-cert     Add binary encoded DER certificate in the registry
  remove-pem-cert  Remove Base 64 encoded PEM certificate from the registry
  add-did          Add DID in the registry
  remove-did       Remove DID from the regitry
  delete-db        Delete db store /!\ Warning : this will result in the loss all of your data
  help             Print this message or the help of the given subcommand(s)

Options:
  -i, --listen-ip <LISTEN_IP>      Listen address [env: IP=] [default: 0.0.0.0]
  -p, --listen-port <LISTEN_PORT>  Listen port [env: PORT=] [default: 8080]
  -s, --db-path <DB_PATH>          Path to db file [env: DB_PATH=] [default: store.db]
  -h, --help                       Print help information
  -V, --version                    Print version information
```

**For security reasons, the server works in read only mode**.
To add the DID or the certificate of a trusted issuer, the HTTP server must be shut down.

Then the first step should be to add a certificate or DID in the store (note that yoy can create many store you want, for exemple for staging, prod, test, ...).
A default store named store.db will be automatically created at first add.

You can add certificate in both PEM and DER format.

In case of multiple certificates in the same PEM file, all of them will be imported sequentially (the order in the file is not important).
```bash
❯ ./trusted-registry add-pem-cert -f test.pem -s "https://www.w3.org/2018/credentials/v1" -s "https://www.gaiax.org/2021/participant/v1"
```

Then you can start the server 
```bash
❯ .trusted-registry serve
[2022-09-23T22:56:44Z INFO  trusted_registry::http] Get Swagger API specs on : http://0.0.0.0:8080/swagger-ui/
[2022-09-23T22:56:44Z INFO  actix_server::builder] Starting 10 workers
[2022-09-23T22:56:44Z INFO  actix_server::server] Actix runtime found; starting in Actix runtime
```

And request the list of all trusted certificates (on `GET /certs`):
```json
{
  "trusted_certs": [
    {
      "dn": "2.5.4.3:solutions.im",
      "cert": {
        "pem_base64": "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tTUlJR0xEQ0NCUlNnQXdJQkFnSVNBMHpkaTRWUjBqaFBkY3duYVJPSnFJOHNNQTBHQ1NxR1NJYjNEUUVCQ3dVQU1ESXhDekFKQmdOVkJBWVRBbFZUTVJZd0ZBWURWUVFLRXcxTVpYUW5jeUJGYm1OeWVYQjBNUXN3Q1FZRFZRUURFd0pTTXpBZUZ3MHlNakE1TURJeE1UTTBNVEphRncweU1qRXlNREV4TVRNME1URmFNQmN4RlRBVEJnTlZCQU1UREhOdmJIVjBhVzl1Y3k1cGJUQ0NBaUl3RFFZSktvWklodmNOQVFFQkJRQURnZ0lQQURDQ0Fnb0NnZ0lCQU5lU1lpQjRJeGgzRGc0TC8rbG1HR3pVbFBYS3REdlA3eXB1UmQvbVFIQ0RIdXgrMG9KTGM5MHZtMzVKOWZiajMyVEZuNk1laG1kUjA5VDRxR04xRVp2blZML2piYjUrcGdhNEw3aVh2UkdHUHplU3RRRUdTcGExdjRoNUgxVENwL09pYVowUkpOcWZ2ZjRJUGJLV3F5UHVxSTN2WmZ5QXRBTHZ2bjlSeW56ajE4bTNSRTEwWTArWmtBZUh3bXNrTTFvcVhVai8yTVNFKzdPUVlBZkJ5Wm01YlZpcjNEY0xDOTlZd0NsQzc1M3pUKzdlb2NMK0R5czlEbndvenV0YUwyLzYxSlB0bDVUTUZvK3pWbGR0b2d3RkdwV0xRZG0yaGE1djcweVl4RFJaYk5GMWt1allJNHFEQjFPUlVobGFGRVYwZ0wzVUNTeW9NNHJhQ3dVRDllYXZZMm5vcTd4dFpxNGxmcjZyYlAzT3M1clVmSHc4VHBvMHk2M0k1TFRYcEE3RGZUQitWNDVMR3hxM0xjMGE1OHBLOVZmeVJBbElsVGllbDJYSWFtc0lNbXBRSGlLV2w4SFk1YW0vaVlvbVpaU2Y2T0hyNFhaUUE2SW1GTVVtL1VKVFBDeVVGOFdYdXNFUmtwMFcwU3l4SXQvVTlOU3JQdXVsblNIeGZMemZuR1IwdWI1cmlEZ1VGYWdVZ3BrZkZQdUpEc1VYOVVJTEdjN2l6S3p3TzA4b0RneXlXTHJzWWRLQ1QxNENoNnNhVFZzSFYxSzRiTjl2cDlvZnJFQnc0NXc5UjFPY2RNdU5YeCtUYVFrQUYwc3QwQ204Njc1a2ZnTnA3eTJrWDdJdVpicTh2ODRvczRFYmRRaGxITjlEaGRmN0xtWUE2aE9DTjlyUk90cUkvbUtSQWdNQkFBR2pnZ0pWTUlJQ1VUQU9CZ05WSFE4QkFmOEVCQU1DQmFBd0hRWURWUjBsQkJZd0ZBWUlLd1lCQlFVSEF3RUdDQ3NHQVFVRkJ3TUNNQXdHQTFVZEV3RUIvd1FDTUFBd0hRWURWUjBPQkJZRUZBQ3FDZXVMNmJXWmthai8rRjg5aXFCbXh0dnJNQjhHQTFVZEl3UVlNQmFBRkJRdXN4ZTNXRmJMcmxBSlFPWWZyNTJMRk1MR01GVUdDQ3NHQVFVRkJ3RUJCRWt3UnpBaEJnZ3JCZ0VGQlFjd0FZWVZhSFIwY0RvdkwzSXpMbTh1YkdWdVkzSXViM0puTUNJR0NDc0dBUVVGQnpBQ2hoWm9kSFJ3T2k4dmNqTXVhUzVzWlc1amNpNXZjbWN2TUNjR0ExVWRFUVFnTUI2Q0Rpb3VjMjlzZFhScGIyNXpMbWx0Z2d4emIyeDFkR2x2Ym5NdWFXMHdUQVlEVlIwZ0JFVXdRekFJQmdabmdRd0JBZ0V3TndZTEt3WUJCQUdDM3hNQkFRRXdLREFtQmdnckJnRUZCUWNDQVJZYWFIUjBjRG92TDJOd2N5NXNaWFJ6Wlc1amNubHdkQzV2Y21jd2dnRUNCZ29yQmdFRUFkWjVBZ1FDQklIekJJSHdBTzRBZFFEZnBWNnJhSUpQSDJ5dDdyaGZUajVhNnMyaUVxUnFYbzQ3RXNBZ1JGd3Fjd0FBQVlMK01jejRBQUFFQXdCR01FUUNJSFdla2VWYkx5OFE0NTNENmF4OGUxdHo2MFh1UXBEV0gzRG9uRXNCL1ROMUFpQmV0VjdIbS8xRy8rTDgzN3A0QzFNZTgyaGg4TXdRZnZtT09CakVhSW4veGdCMUFDbDV2dkNlT1RraDhGWnpuMk9sZCtXK1YzMmNZQXI0K1UxZEpsd2xYY2VFQUFBQmd2NHh6UEVBQUFRREFFWXdSQUlnZFNJMjFsalFIMXZoYkEvbFlya1gzNWJUZHFUOTBoM2s0NzNvcFgydW1KRUNJQjhKVmlPS08xRWpUREY2WnhTK2VtOURNRm5FR2VkKzdsamQzMXd3MGZqK01BMEdDU3FHU0liM0RRRUJDd1VBQTRJQkFRQ0lZRVFpbDBhK240VTBEVkxEbWRsTDdLeklkWU50TG5zR2p0R0ZURmRWN25lYVFvOXdleERBdCtsN3BBZ3hhZ09lWHEvWEJkbWt2MDRxYjU5TGRxK2sxcGNJYk9XYzlSZ1NMdEpHSlNnU3kzNjNhK2k2WVc3QmhqMzkwRlg2M2xXMXgrTkMySFpMMkNscllacGl3MnF3VkMvaDRzVlZwdkZVRDkxZ09iOTNEUE80UXpFeUIrZ3ZTRjJ6dXFBNFJkb0xmZVpsUU9XbkhqOVRXdlF5U1ZKZDVpOW4wUnhlVTlTNTliSTVoUHIycmxCTDlDczhKYWx2MG52bmFjdlE5di9jazhjV0VJc2EzcFh1Ulc1NlVYWWNTaVViTHpjZzdpTFAwV1JLYURyaUhJSmd4TytHUkVBQTJCVHlXTjRiOGtYelBjOVhJY3FzMGkyc280KzFBN21nLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQ=="
      },
      "trusted_for_context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.gaiax.org/2021/participant/v1clear"
      ]
    },
    {
      "dn": "2.5.4.6:US,2.5.4.10:Internet Security Research Group,2.5.4.3:ISRG Root X1",
      "cert": {
        "pem_base64": "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUZZRENDQkVpZ0F3SUJBZ0lRUUFGM0lUZlU2VUs0N25hcVBHUUt0ekFOQmdrcWhraUc5dzBCQVFzRkFEQS8KTVNRd0lnWURWUVFLRXh0RWFXZHBkR0ZzSUZOcFoyNWhkSFZ5WlNCVWNuVnpkQ0JEYnk0eEZ6QVZCZ05WQkFNVApEa1JUVkNCU2IyOTBJRU5CSUZnek1CNFhEVEl4TURFeU1ERTVNVFF3TTFvWERUSTBNRGt6TURFNE1UUXdNMW93ClR6RUxNQWtHQTFVRUJoTUNWVk14S1RBbkJnTlZCQW9USUVsdWRHVnlibVYwSUZObFkzVnlhWFI1SUZKbGMyVmgKY21Ob0lFZHliM1Z3TVJVd0V3WURWUVFERXd4SlUxSkhJRkp2YjNRZ1dERXdnZ0lpTUEwR0NTcUdTSWIzRFFFQgpBUVVBQTRJQ0R3QXdnZ0lLQW9JQ0FRQ3Q2Q1J6OUJRMzg1dWVLMWNvSEllKzNMZmZPSkNNYmp6bVY2QjQ5M1hDCm92NzFhbTcyQUU4bzI5NW9obXhFazdheFkvMFVFbXUvSDlMcU1ac2hmdEV6UExwSTlkMTUzN080L3hMeElacEwKd1lxR2NXbEtabVpzajM0OGNMK3RLU0lHOCtUQTVvQ3U0a3VQdDVsK2xBT2YwMGVYZkpsSUkxUG9PSzVQQ20rRApMdEZKVjR5QWRMYmFMOUE0alhzRGNDRWJkZkl3UFBxUHJ0M2FZNnZyRmsvQ2poRkxmczhMNlArMWR5NzBzbnRLCjRFd1NKUXh3alFNcG9PRlRKT3dUMmU0WnZ4Q3pTb3cvaWFOaFVkNnNod2VVOUdOeDdDN2liMXVZZ2VHSlhEUjUKYkhidk81QmllZWJicEpvdkpzWFFFT0VPM3RrUWpoYjd0L2VvOThmbEFnZVlqellJbGVmaU41WU5ObldlK3c1eQpzUjJidkFQNVNRWFlnZDBGdENyV1FlbXNBWGFWQ2cvWTM5VzlFaDgxTHlnWGJOS1l3YWdKWkhkdVJ6ZTZ6cXhaClhtaWRmM0xXaWNVR1FTaytXVDdkSnZVa3lSR25XcU5NUUI5R29abTFwenBSYm9ZN25uMXlweElGZUZudFBsRjQKRlFzRGo0M1FMd1d5UG50S0hFdHpCUkw4eHVyZ1VCTjhRNU4wczhwMDU0NGZBUWpRTU5SYmNUYTBCN3JCTURCYwpTTGVDTzVpbWZXQ0tvcU1wZ3N5NnZZTUVHNktEQTBHaDFnWHhHOEsyOEtoOGhqdEdxRWdxaU54Mm1uYS9IMnFsClBSbVA2emp6Wk43SUt3MEtLUC8zMitJVlF0UWkwQ2RkNFhuK0dPZHdpSzFPNXRtTE9zYmRKMUZ1Lzd4azlUTkQKVHdJREFRQUJvNElCUmpDQ0FVSXdEd1lEVlIwVEFRSC9CQVV3QXdFQi96QU9CZ05WSFE4QkFmOEVCQU1DQVFZdwpTd1lJS3dZQkJRVUhBUUVFUHpBOU1Ec0dDQ3NHQVFVRkJ6QUNoaTlvZEhSd09pOHZZWEJ3Y3k1cFpHVnVkSEoxCmMzUXVZMjl0TDNKdmIzUnpMMlJ6ZEhKdmIzUmpZWGd6TG5BM1l6QWZCZ05WSFNNRUdEQVdnQlRFcDdHa2V5eHgKK3R2aFM1QjEvOFFWWUlXSkVEQlVCZ05WSFNBRVRUQkxNQWdHQm1lQkRBRUNBVEEvQmdzckJnRUVBWUxmRXdFQgpBVEF3TUM0R0NDc0dBUVVGQndJQkZpSm9kSFJ3T2k4dlkzQnpMbkp2YjNRdGVERXViR1YwYzJWdVkzSjVjSFF1CmIzSm5NRHdHQTFVZEh3UTFNRE13TWFBdm9DMkdLMmgwZEhBNkx5OWpjbXd1YVdSbGJuUnlkWE4wTG1OdmJTOUUKVTFSU1QwOVVRMEZZTTBOU1RDNWpjbXd3SFFZRFZSME9CQllFRkhtMFdlWjd0dVhrQVhPQUNJaklHbGoyNlp0dQpNQTBHQ1NxR1NJYjNEUUVCQ3dVQUE0SUJBUUFLY3dCc2xtNy9EbExRcnQyTTUxb0dyUytvNDQrL3lRb0RGVkRDCjVXeEN1MitiOUxSUHdrU0lDSFhNNndlYkZHSnVlTjdzSjdvNVhQV2lvVzVXbEhBUVU3Rzc1Sy9Rb3NNckFkU1cKOU1VZ05UUDUyR0UyNEhHTnRMaTFxb0pGbGNEeXFTTW81OWFoeTJjSTJxQkRMS29ia3gvSjN2V3JhVjBUOVZ1RwpXQ0xLVFZYa2NHZHR3bGZGUmpsQno0cFlnMWh0bWY1WDZEWU84QTRqcXYySWw5RGpYQTZVU2JXMUZ6WFNMcjlPCmhlOFk0SVdTNndZN2JDa2pDV0RjUlFKTUVoZzc2ZnNPM3R4RStGaVlydXE5UlVXaGlGMW15djRRNlcrQ3lCRkMKRGZ2cDdPT0dBTjZkRU9NNCtxUjlzZGpvU1lLRUJwc3I2R3RQQVF3NGR5NzUzZWM1Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0="
      },
      "trusted_for_context": []
    },
    {
      "dn": "2.5.4.6:US,2.5.4.10:Let's Encrypt,2.5.4.3:R3",
      "cert": {
        "pem_base64": "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUZGakNDQXY2Z0F3SUJBZ0lSQUpFckNFclBEQmluVS9iV0xpV25YMW93RFFZSktvWklodmNOQVFFTEJRQXcKVHpFTE1Ba0dBMVVFQmhNQ1ZWTXhLVEFuQmdOVkJBb1RJRWx1ZEdWeWJtVjBJRk5sWTNWeWFYUjVJRkpsYzJWaApjbU5vSUVkeWIzVndNUlV3RXdZRFZRUURFd3hKVTFKSElGSnZiM1FnV0RFd0hoY05NakF3T1RBME1EQXdNREF3CldoY05NalV3T1RFMU1UWXdNREF3V2pBeU1Rc3dDUVlEVlFRR0V3SlZVekVXTUJRR0ExVUVDaE1OVEdWMEozTWcKUlc1amNubHdkREVMTUFrR0ExVUVBeE1DVWpNd2dnRWlNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0SUJEd0F3Z2dFSwpBb0lCQVFDN0FoVW96UGFnbE5NUEV1eU5WWkxEK0lMeG1hWjZRb2luWFNhcXRTdTV4VXl4cjQ1citYWElvOWNQClI1UVVWVFZYako2b29qa1o5WUk4UXFsT2J2VTd3eTdiamNDd1hQTlpPT2Z0ejJud1dnc2J2c0NVSkNXSCtqZHgKc3hQbkhLemhtKy9iNUR0RlVrV1dxY0ZUempUSVV1NjFydTJQM21CdzRxVlVxN1p0RHBlbFFEUnJLOU84WnV0bQpOSHo2YTR1UFZ5bVorREFYWGJweWIvdUJ4YTNTaGxnOUY4Zm5DYnZ4Sy9lRzNNSGFjVjNVUnVQTXJTWEJpTHhnClozVm1zL0VZOTZKYzVsUC9Pb2kyUjZYL0V4anFtQWwzUDUxVCtjOEI1ZldtY0JjVXIyT2svNW16azUzY1U2Y0cKL2tpRkhhRnByaVYxdXhQTVVnUDE3VkdoaTlzVkFnTUJBQUdqZ2dFSU1JSUJCREFPQmdOVkhROEJBZjhFQkFNQwpBWVl3SFFZRFZSMGxCQll3RkFZSUt3WUJCUVVIQXdJR0NDc0dBUVVGQndNQk1CSUdBMVVkRXdFQi93UUlNQVlCCkFmOENBUUF3SFFZRFZSME9CQllFRkJRdXN4ZTNXRmJMcmxBSlFPWWZyNTJMRk1MR01COEdBMVVkSXdRWU1CYUEKRkhtMFdlWjd0dVhrQVhPQUNJaklHbGoyNlp0dU1ESUdDQ3NHQVFVRkJ3RUJCQ1l3SkRBaUJnZ3JCZ0VGQlFjdwpBb1lXYUhSMGNEb3ZMM2d4TG1rdWJHVnVZM0l1YjNKbkx6QW5CZ05WSFI4RUlEQWVNQnlnR3FBWWhoWm9kSFJ3Ck9pOHZlREV1WXk1c1pXNWpjaTV2Y21jdk1DSUdBMVVkSUFRYk1Ca3dDQVlHWjRFTUFRSUJNQTBHQ3lzR0FRUUIKZ3Q4VEFRRUJNQTBHQ1NxR1NJYjNEUUVCQ3dVQUE0SUNBUUNGeWs1SFBxUDNoVVNGdk5WbmVMS1lZNjExVFI2VwpQVE5sY2xRdGdhRHF3KzM0SUw5ZnpMZHdBTGR1Ty9aZWxON2tJSittNzR1eUErZWl0Ulk4a2M2MDdUa0M1M3dsCmlrZm1aVzQvUnZUWjhNNlVLKzVVemhLOGpDZEx1TUdZTDZLdnpYR1JTZ2kzeUxnamV3UXRDUGtJVno2RDJRUXoKQ2tjaGVBbUNKOE1xeUp1NXpsenlaTWpBdm5uQVQ0NXRSQXhla3JzdTk0c1E0ZWdkUkNuYldTRHRZN2toK0JJbQpsSk5Yb0IxbEJNRUtJcTRRRFVPWG9SZ2ZmdURnaGplMVdyRzlNTCtIYmlzcS95Rk9Hd1hEOVJpWDhGNnN3Nlc0CmF2QXV2RHN6dWU1TDNzejg1SytFQzRZL3dGVkROdlpvNFRZWGFvNlowZitsUUtjMHQ4RFFZemsxT1hWdThycDIKeUpNQzZhbExiQmZPREFMWnZZSDduN2RvMUFabHM0STlkMVA0am5rRHJRb3hCM1VxUTloVmwzTEVLUTczeEYxTwp5SzVHaEREWDhvVmZHS0Y1dStkZWNJc0g0WWFUdzdtUDNHRnhKU3F2MyswbFVGSm9pNUxjNWRhMTQ5cDkwSWRzCmhDRXhyb0wxKzdtcnlJa1hQZUZNNVRnTzlyMHJ2WmFCRk92VjJ6MGdwMzVaMCtMNFdQbGJ1RWpOL2x4UEZpbisKSGxVanI4Z1JzSTNxZkpPUUZ5LzlyS0lKUjBZLzhPbXd0LzhvVFdneTFtZGVIbW1qazdqMW5Zc3ZDOUpTUTZadgpNbGRsVFRLQjN6aFRoVjErWFdZcDZyamQ1SlcxemJWV0VrTE54RTdHSlRoRVVHM3N6Z0JWR1A3cFNXVFVUc3FYCm5MUmJ3SE9vcTdoSHdnPT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQ=="
      },
      "trusted_for_context": []
    }
  ]
}
```

Exemple on `GET /dids` :
```json
{
  "trusted_dids": [
    {
      "did": "did:web:mycompany.gaiax.org",
      "trusted_for_context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.gaiax.org/2021/participant/v1"
      ]
    },
    {
      "did": "did:web:test.com",
      "trusted_for_context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.gaiax.org/2021/participant/v1"
      ]
    }
  ]
}
```

## API

The OpenAPI description file can be downloaded from : `http(s)://your_server_address:your_server_port/api-doc/openapi.json`

A swagger UI loaded with Trusted-Registry API description is available from : `http(s)://your_server_address:your_server_port/swagger-ui/`

## Metrics 

The prometheus metrics is available from : `http(s)://your_server_address:your_server_port/metrics`

Ex : 
```bash
# HELP api_http_requests_duration_seconds HTTP request duration in seconds for all requests
# TYPE api_http_requests_duration_seconds histogram
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="0.005"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="0.01"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="0.025"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="0.05"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="0.1"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="0.25"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="0.5"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="1"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="2.5"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="5"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="10"} 4
api_http_requests_duration_seconds_bucket{endpoint="/api-doc/openapi.json",method="GET",status="200",le="+Inf"} 4
...
```
