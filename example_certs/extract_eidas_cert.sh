# Get XML file from https://ec.europa.eu/tools/lotl/eu-lotl.xml
yq e -p=xml '.TrustServiceStatusList.SchemeInformation.PointersToOtherTSL.OtherTSLPointer.[].ServiceDigitalIdentities.[]' eu-lotl.xml > cert_eidas
cat cert_eidas | grep X509Certificate | awk '{print "-----BEGIN CERTIFICATE-----"$2"-----END CERTIFICATE-----"}' > certs_eidas.pem