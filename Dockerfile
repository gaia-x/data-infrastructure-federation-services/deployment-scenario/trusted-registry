FROM scratch
ARG TARGETOS
ARG TARGETARCH
COPY /${TARGETOS}/${TARGETARCH}/trusted-registry /
WORKDIR /
CMD ["/trusted-registry", "serve"]