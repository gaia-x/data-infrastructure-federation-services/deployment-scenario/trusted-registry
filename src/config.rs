use std::fmt;

use clap::{command, Parser, Subcommand, ValueEnum};
use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

use crate::store::data::{TrustedCerts, TrustedDIDs};

#[derive(Serialize, Deserialize, Debug, Clone, ToSchema)]
pub(crate) struct ConfigData {
    pub listen_on: String,
    pub store_file_path: String,
    pub authorized_dids: TrustedDIDs,
    pub authorized_certs: TrustedCerts,
}

#[derive(Parser, Debug)]
#[command(author, version, about = "A simple trusted registry", long_about = None)]
#[command(propagate_version = true)]
pub(super) struct Args {
    /// Listen address
    #[arg(short = 'i', long, value_parser, default_value = "0.0.0.0", env = "IP")]
    pub(super) listen_ip: String,

    /// Listen port
    #[arg(short = 'p', long, value_parser, default_value_t = 8080, env = "PORT")]
    pub(super) listen_port: u16,

    /// Path to db file
    #[arg(
        short = 's',
        long,
        value_parser,
        default_value = "store.db",
        env = "DB_PATH"
    )]
    pub(super) db_path: String,

    /// Command to launch between
    #[clap(value_enum, subcommand)]
    pub(super) command: Operation,
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Subcommand)]
pub(super) enum Operation {
    /// Launch Trusted Registry server as read only mode
    Serve {
        /// Log level
        #[arg(short, value_enum, default_value_t = LogLevel::Info)]
        log_level: LogLevel,

        /// Read only of read write mode
        #[arg(short, value_enum, default_value_t = Mode::ReadOnly)]
        mode: Mode,
    },
    /// Add Base 64 encoded PEM certificate in the registry
    AddPEMCert {
        /// Path to PEM file
        #[arg(short, long)]
        file_path: String,
        /// Scope of trust (I.E. the schema(s) for which the DID can issue VC),
        /// multiple value accepted, ex : -s "https://www.w3.org/2018/credentials/v1" -s "https://www.gaiax.org/2021/participant/v1"
        #[arg(short, long)]
        scope: Vec<String>,
    },
    /// Add binary encoded DER certificate in the registry
    AddDERCert {
        /// Path to the DER file
        #[arg(short, long)]
        file_path: String,
        /// Scope of trust (I.E. the schema(s) for which the DID can issue VC),
        /// multiple value accepted, ex : -s "https://www.w3.org/2018/credentials/v1" -s "https://www.gaiax.org/2021/participant/v1"
        #[arg(short, long)]
        scope: Vec<String>,
    },
    /// Remove Base 64 encoded PEM certificate from the registry
    RemovePEMCert {
        /// Base64 PEM encoded version of certificate to delete
        #[arg(short, long)]
        base_64_pem_cert: String,
    },
    /// Add DID in the registry
    AddDID {
        /// Did to add
        #[arg(short, long)]
        did: String,
        /// Scope of trust (I.E. the schema(s) for which the DID can issue VC),
        /// multiple value accepted, ex : -s "https://www.w3.org/2018/credentials/v1" -s "https://www.gaiax.org/2021/participant/v1"
        #[arg(short, long)]
        scope: Vec<String>,
    },
    /// Remove DID from the regitry
    RemoveDID {
        /// Did to remove
        #[arg(short, long)]
        did: String,
    },
    /// Delete db store /!\ Warning : this will result in the loss all of your data
    DeleteDB {
        #[arg(value_enum)]
        confirm: Confirm,
    },
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug, ValueEnum)]
pub(super) enum Confirm {
    Yes,
    No,
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug, ValueEnum)]
pub(super) enum LogLevel {
    Debug,
    Info,
    Warn,
    Error,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, ValueEnum)]
pub(super) enum Mode {
    ReadOnly,
    ReadWrite,
}

impl fmt::Display for LogLevel {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            LogLevel::Debug => write!(f, "debug"),
            LogLevel::Info => write!(f, "info"),
            LogLevel::Warn => write!(f, "warning"),
            LogLevel::Error => write!(f, "error"),
        }
    }
}
