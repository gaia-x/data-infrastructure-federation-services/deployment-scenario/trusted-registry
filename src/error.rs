use bcder::decode::DecodeError;
use std::io;
use thiserror::Error;
use x509_certificate::X509CertificateError;

#[derive(Error, Debug)]
pub enum Error {
    #[error("error with storage layer")]
    Storage(#[from] sled::Error),
    #[error("no opened datastore")]
    NoOpenStore,
    #[error("IO error")]
    IOError(#[from] io::Error),
    #[error("can't get db size")]
    DbSize,
    #[error("unable to convert db value")]
    ValueConvertion(#[from] std::str::Utf8Error),
    #[error("unable to serialize struct")]
    SerialisationError(#[from] bincode::Error),

    #[error("unable to parse x509 certificate")]
    X509CertificateError(#[from] X509CertificateError),
    #[error("unable to parse certificate")]
    CertificateImportError(String),
    #[error("unable to extract attribute")]
    CertificateAttributeError(#[from] DecodeError<std::convert::Infallible>),
    #[error("certificate not found in trusted db")]
    CertificateNotFound,

    #[error("unable to parse did")]
    DIDParseError(String),
}

impl actix_web::error::ResponseError for Error {}
