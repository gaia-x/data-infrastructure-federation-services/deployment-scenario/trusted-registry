use actix_web::error::ErrorInternalServerError;
use actix_web::web::Json;
use actix_web::{put, web, Error};

use crate::cert::CertUtil;
use crate::error;
use crate::store::data::{CertPut, CertPutResult, DidPutResult, DID};
use crate::store::local_store::LocalStore;
use crate::store::TrustedPersistence;

/// Put a trusted certificate in PEM format with his trust scope.
///
/// Only one cert per request is allowed.
#[utoipa::path(
    request_body(content = CertPut, description = "The certificate to add"),
    responses(
        (status = 200, description = "Put a new certificate in the trusted registry", body = [CertPutResult]),
    ),
    tag = "certs",
    path = "/api/v1/certs"
)]
#[put("/cert")]
pub(crate) async fn put_trusted_cert(
    content: web::Data<LocalStore>,
    body: web::Json<CertPut>,
) -> Result<Json<CertPutResult>, Error> {
    let cert_decoded =
        base64::decode(&body.pem_cert).map_err(|_| ErrorInternalServerError("Invalid base64"))?;
    let cert_to_put_decoded =
        x509_certificate::CapturedX509Certificate::from_pem_multiple(cert_decoded)
            .map_err(ErrorInternalServerError)?;

    let to_store: Vec<(String, Vec<u8>)> = cert_to_put_decoded
        .into_iter()
        .map(|cert| {
            let dn = cert.get_dn();
            let der = cert.encode_der().map_err(error::Error::IOError);
            (dn, der, cert)
        })
        .map(|dn_and_cert| match dn_and_cert {
            (Ok(dn), Ok(der), _) => Ok((dn, der)),
            (Ok(_), Err(e), cert) => Err(error::Error::CertificateImportError(format!(
                "{},for cert : {:?}",
                e,
                cert.encode_pem()
            )))
            .map_err(actix_web::error::ErrorInternalServerError),
            (Err(e), Ok(_), cert) => Err(error::Error::CertificateImportError(format!(
                "{},for cert : {:?}",
                e,
                cert.encode_pem()
            )))
            .map_err(actix_web::error::ErrorInternalServerError),
            (Err(e1), Err(e2), cert) => Err(error::Error::CertificateImportError(format!(
                "{} {}, for cert {:?}",
                e1,
                e2,
                cert.encode_pem()
            )))
            .map_err(actix_web::error::ErrorInternalServerError),
        })
        .collect::<Result<Vec<(String, Vec<u8>)>, actix_web::error::Error>>()?;

    to_store.into_iter().try_for_each(|cert| {
        content
            .put_new_cert(cert.0, cert.1, body.trusted_for_context.clone())
            .map_err(actix_web::error::ErrorInternalServerError)
    })?;

    Ok(web::Json(CertPutResult {
        status: "imported".to_owned(),
    }))
}

/// Put a trusted DID with his trust scope.
#[utoipa::path(
    request_body(content = DID, description = "The DID to add"),
    responses(
        (status = 200, description = "Put a new DID in the trusted registry", body = [DidPutResult]),
    ),    
    tag = "dids",
    path = "/api/v1/did"
)]
#[put("/did")]
pub(crate) async fn put_trusted_did(
    content: web::Data<LocalStore>,
    body: web::Json<DID>,
) -> Result<Json<DidPutResult>, Error> {
    content
        .put_new_did(body.did.clone(), body.trusted_for_context.clone())
        .map_err(actix_web::error::ErrorInternalServerError)?;

    Ok(web::Json(DidPutResult {
        status: "imported".to_owned(),
    }))
}

#[cfg(test)]
mod test {
    use crate::{methods::read_write, store::local_store::LocalStore};
    use actix_web::{test, web::Data, App};

    const EMPTY_STORE_PATH: &str = "empty.db";

    #[actix_web::test]
    async fn test_put_cert() {
        let mut store = LocalStore::new(EMPTY_STORE_PATH.to_owned());
        let open = store.open();
        assert!(open.is_ok());
        let container_store = Data::new(store);

        let app = test::init_service(
            App::new()
                .app_data(container_store.clone())
                .configure(read_write(container_store.clone())),
        )
        .await;
        let solutions = include_str!("../../example_certs/solutions-im.pem.base64");

        let req = test::TestRequest::put()
            .uri("/api/v1/cert")
            .set_json(&crate::store::data::CertPut {
                pem_cert: solutions.to_owned(),
                trusted_for_context: vec!["".to_owned()],
            })
            .to_request();

        let resp = test::call_service(&app, req).await;
        println!("{:?}", resp);
        assert!(resp.status().is_success());
    }
}
