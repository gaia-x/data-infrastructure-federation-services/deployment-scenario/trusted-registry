use crate::{
    cert::CertUtil,
    config::Confirm,
    error,
    store::{data::CertFormat, local_store::LocalStore, TrustedPersistence},
};

pub(crate) async fn add_did(
    store_path: String,
    did: String,
    scope: Vec<String>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut store = LocalStore::new(store_path);
    store.open()?;
    store.put_new_did(did, scope).map_err(|e| e.into())
}

pub(crate) async fn remove_did(
    store_path: String,
    did: String,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut store = LocalStore::new(store_path);
    store.open()?;
    store.remove_did(&did).map_err(|e| e.into())
}

pub(crate) async fn add_cert(
    store_path: String,
    cert_encoded: CertFormat,
    scope: Vec<String>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut store = LocalStore::new(store_path);

    let to_store: Vec<(String, Vec<u8>)> =
        match cert_encoded {
            CertFormat::PemBase64(pem) => {
                let decoded = x509_certificate::CapturedX509Certificate::from_pem_multiple(pem)?;

                decoded
                    .into_iter()
                    .map(|cert| {
                        let dn = cert.get_dn();
                        let der = cert.encode_der().map_err(error::Error::IOError);
                        (dn, der, cert)
                    })
                    .map(|dn_and_cert| match dn_and_cert {
                        (Ok(dn), Ok(der), _) => Ok((dn, der)),
                        (Ok(_), Err(e), cert) => Err(error::Error::CertificateImportError(
                            format!("{},for cert : {:?}", e, cert.encode_pem()),
                        )),
                        (Err(e), Ok(_), cert) => Err(error::Error::CertificateImportError(
                            format!("{},for cert : {:?}", e, cert.encode_pem()),
                        )),
                        (Err(e1), Err(e2), cert) => Err(error::Error::CertificateImportError(
                            format!("{} {}, for cert {:?}", e1, e2, cert.encode_pem()),
                        )),
                    })
                    .collect()
            }

            CertFormat::Der(der) => {
                let decoded = x509_certificate::CapturedX509Certificate::from_der(der.clone())?;
                let dn = decoded.get_dn()?;
                Ok(vec![(dn, der)])
            }
        }?;
    store.open()?;
    to_store.into_iter().try_for_each(|cert| {
        store
            .put_new_cert(cert.0, cert.1, scope.clone())
            .map_err(|e| e.into())
    })
}

pub(crate) async fn remove_cert(
    store_path: String,
    cert: String,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut store = LocalStore::new(store_path);
    store.open()?;
    store.remove_cert(&cert).map_err(|e| e.into())
}

pub(crate) async fn delete_db(
    store_path: String,
    confirm: Confirm,
) -> Result<(), Box<dyn std::error::Error>> {
    match confirm {
        Confirm::Yes => {
            let mut store = LocalStore::new(store_path);
            store.open()?;
            store.delete().map_err(|e| e.into())
        }
        Confirm::No => Ok(()),
    }
}
