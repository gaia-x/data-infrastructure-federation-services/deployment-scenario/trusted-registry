use actix_web::error::ErrorInternalServerError;
use actix_web::web::Json;
use actix_web::{get, post, web, Error};
use x509_certificate::CapturedX509Certificate;

use crate::cert::{self, CertUtil};
use crate::store::data::{Cert, CertPost, CheckCertResult, TrustedCerts, TrustedDIDs};
use crate::store::local_store::LocalStore;
use crate::store::TrustedPersistence;

/// Get the list of trusted DIDs.
///
/// Each DID is associated with a list of trust scopes.
#[utoipa::path(
    responses(
        (status = 200, description = "Return the list trusted DIDs", body = [TrustedDIDs])
    ),
    tag = "dids",
    path = "/api/v1/did"
)]
#[get("/did")]
pub(crate) async fn get_trusted_dids(
    content: web::Data<LocalStore>,
) -> Result<Json<TrustedDIDs>, Error> {
    let r = content
        .get_all_dids()
        .map_err(actix_web::error::ErrorInternalServerError)?;
    Ok(web::Json(r))
}

/// Get the list of trusted certificates.
///
/// Each certificate is associated with a list of trust scopes.
#[utoipa::path(
    responses(
        (status = 200, description = "Return the list of trusted Certificates.", body = [TrustedCerts])
    ),
    tag = "certs",
    path = "/api/v1/certs"
)]
#[get("certs")]
pub(crate) async fn get_trusted_certs(
    content: web::Data<LocalStore>,
) -> Result<Json<TrustedCerts>, Error> {
    let r = content
        .get_all_certs()
        .map_err(actix_web::error::ErrorInternalServerError)?;
    Ok(web::Json(r))
}

/// Check if a certificate is trusted.
///
/// Check one PEM certificate against the certificates present in the trusted registry.
/// The issuer of the certificate must be in the trusted registry.
/// If the certificate to test is a chain of certificates, the chain is verified and the last element (closest to the root) is checked against the registry.
/// If `recursive` is set to true, the issuer of the issuer of the certificate must be in the trusted registry.
#[utoipa::path(
    request_body(content = CertPost, description = "The certificate to check"),
    responses(
        (status = 200, description = "Check if the provided certificate is valid against trusted certificate(s)", body = [CheckCertResult])
    ),
    tag = "certs",
    path = "/api/v1/check_cert"
)]
#[post("/check_cert")]
pub(crate) async fn check_cert(
    content: web::Data<LocalStore>,
    body: web::Json<CertPost>,
) -> Result<Json<CheckCertResult>, Error> {
    let certs = x509_certificate::CapturedX509Certificate::from_pem_multiple(
        base64::decode(body.cert.clone()).map_err(|err| {
            ErrorInternalServerError(format!(
                "unable to decode provided base64 PEM certificate : {}",
                err
            ))
        })?,
    )
    .map_err(ErrorInternalServerError)?;
    let cert_to_check = if certs.len() > 1 {
        for i in 0..certs.len() - 1 {
            let cert_to_check = certs.get(i).unwrap();
            let issuer = certs.get(i + 1).unwrap();
            cert_to_check
                .verify_signed_by_certificate(issuer)
                .map_err(|err| {
                    ErrorInternalServerError(format!("Error verifying certificate: {}", err))
                })?;
        }
        certs.last().ok_or_else(|| {
            ErrorInternalServerError("Unable to get last certificate of the chain")
        })?
    } else {
        certs
            .first()
            .ok_or_else(|| ErrorInternalServerError("unable to get cert from PEM content"))?
    };
    let issuer_dn = cert_to_check.get_issuer_dn()?;
    let issuer = content.retrieve_cert_by_dn(&issuer_dn)?;

    check(
        cert_to_check.to_owned(),
        issuer_dn,
        issuer,
        body.recursive,
        content,
    )
}

fn check(
    cert_to_check: CapturedX509Certificate,
    issuer_dn: String,
    issuer: Option<Cert>,
    recursive: bool,
    content: web::Data<LocalStore>,
) -> Result<Json<CheckCertResult>, Error> {
    match issuer {
        Some(issuer) => {
            let pem = issuer.cert.to_pem()?;
            let result = match cert::check_cert(cert_to_check.encode_pem().as_str(), &pem) {
                Ok(_) => Ok(web::Json(CheckCertResult {
                    check_result_detail: format!(
                        "Certificate verified against issuer {}",
                        issuer_dn
                    ),
                    check_result: true,
                })),
                Err(err) => Err(err.into()),
            };
            if recursive && !cert_to_check.subject_is_issuer() {
                let cert_tp_check =
                    x509_certificate::CapturedX509Certificate::from_pem(pem.as_bytes())
                        .map_err(ErrorInternalServerError)?;
                let issuer_dn = cert_tp_check.get_issuer_dn()?;
                let issuer = content.retrieve_cert_by_dn(&issuer_dn)?;
                match issuer {
                    Some(_) => check(cert_tp_check, issuer_dn, issuer, recursive, content),
                    None => {
                        if cert_to_check.subject_is_issuer() {
                            Ok(web::Json(CheckCertResult {
                                check_result_detail: format!(
                                    "Certificate verified against issuer {}",
                                    issuer_dn
                                ),
                                check_result: true,
                            }))
                        } else {
                            Ok(web::Json(CheckCertResult {
                                check_result_detail:
                                    "Issuer certificate not in trusted certificates".to_string(),
                                check_result: false,
                            }))
                        }
                    }
                }
            } else {
                result
            }
        }
        None => Err(crate::error::Error::CertificateNotFound.into()),
    }
}

#[cfg(test)]
mod test {
    use actix_web::web::Data;

    use super::*;
    use crate::{
        methods::{cli::add_cert, read_only},
        store::local_store::LocalStore,
    };
    use actix_web::{body, test, App};
    const NON_RECURSIVE_STORE_PATH: &str = "non_recursive.db";
    const RECURSIVE_STORE_PATH: &str = "recursive.db";
    const CHAIN_CERT_STORE: &str = "chain_cert.db";

    async fn test_check(recursive: bool, check_result: bool, store_path: String) {
        let mut store = LocalStore::new(store_path.clone());

        let root = include_str!("../../example_certs/root-ca.pem");
        let intermediate = include_str!("../../example_certs/intermediate.pem");
        let solutions = include_str!("../../example_certs/solutions-im.pem");

        println!("Adding root certificate");
        let add_root = add_cert(
            store_path.clone(),
            crate::store::data::CertFormat::PemBase64(root.to_owned()),
            vec!["".to_owned()],
        )
        .await;
        assert!(add_root.is_ok());

        println!("Adding intermediate certificate");
        let add_intermediate = add_cert(
            store_path.clone(),
            crate::store::data::CertFormat::PemBase64(intermediate.to_owned()),
            vec!["".to_owned()],
        )
        .await;
        assert!(add_intermediate.is_ok());

        println!("Adding solutions certificate");
        let add_solutions = add_cert(
            store_path,
            crate::store::data::CertFormat::PemBase64(solutions.to_owned()),
            vec!["".to_owned()],
        )
        .await;
        assert!(add_solutions.is_ok());

        let open = store.open();
        assert!(open.is_ok());
        let container_store = Data::new(store);

        let app = test::init_service(
            App::new()
                .app_data(container_store.clone())
                .configure(read_only(container_store.clone())),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/api/v1/check_cert")
            .set_json(&crate::store::data::CertPost {
                cert: base64::encode(solutions),
                recursive,
            })
            .to_request();

        let resp = test::call_service(&app, req).await;
        println!("{:?}", resp);
        assert!(resp.status().is_success());
        let body = resp.into_body();
        let byte_body = body::to_bytes(body).await;
        assert!(byte_body.is_ok());
        let byte_body = byte_body.unwrap();
        let json_resp: CheckCertResult = serde_json::from_slice(&byte_body).unwrap();
        assert!(json_resp.check_result == check_result);
        println!("{:#?}", json_resp);
    }

    #[actix_web::test]
    async fn test_check_chain_of_cert() {
        let test_cert = include_str!("../../example_certs/test.pem");
        let test_cert = base64::encode(test_cert);

        // Start by adding only root cert on store
        let mut store = LocalStore::new(CHAIN_CERT_STORE.to_owned());

        println!("Adding root X3 certificate");
        let root = include_str!("../../example_certs/root-x3.pem");
        let add_root = add_cert(
            CHAIN_CERT_STORE.to_owned(),
            crate::store::data::CertFormat::PemBase64(root.to_owned()),
            vec!["".to_owned()],
        )
        .await;
        assert!(add_root.is_ok());

        let open = store.open();
        assert!(open.is_ok());

        let container_store = Data::new(store);
        let app = test::init_service(
            App::new()
                .app_data(container_store.clone())
                .configure(read_only(container_store.clone())),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/api/v1/check_cert")
            .set_json(&crate::store::data::CertPost {
                cert: test_cert.to_owned(),
                recursive: false,
            })
            .to_request();

        let resp = test::call_service(&app, req).await;
        println!("{:?}", resp);
        assert!(resp.status().is_success());
        let body = resp.into_body();
        let byte_body = body::to_bytes(body).await;
        assert!(byte_body.is_ok());
        let byte_body = byte_body.unwrap();
        let json_resp: CheckCertResult = serde_json::from_slice(&byte_body).unwrap();
        assert!(json_resp.check_result);
        println!("{:#?}", json_resp);
    }

    #[actix_web::test]
    async fn test_check_cert_non_recursive() {
        test_check(false, true, NON_RECURSIVE_STORE_PATH.to_owned()).await
    }

    #[actix_web::test]
    async fn test_check_cert_recursive() {
        test_check(true, false, RECURSIVE_STORE_PATH.to_owned()).await
    }
}
