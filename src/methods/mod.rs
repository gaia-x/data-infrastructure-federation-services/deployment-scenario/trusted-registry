pub mod cli;
pub mod read_only;
pub mod read_write;

use crate::{
    config::{LogLevel, Mode},
    store::{self, local_store::LocalStore},
};
use actix_web::{middleware::Logger, web::{Data, ServiceConfig, self}, App, HttpServer};
use actix_web_prom::PrometheusMetricsBuilder;
use log::info;
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

use self::{read_only::{get_trusted_certs, get_trusted_dids, check_cert}, read_write::{put_trusted_cert, put_trusted_did}};

pub(super) async fn serve(
    addr: String,
    port: u16,
    store_path: String,
    mode: Mode,
    log_level: LogLevel,
) -> Result<(), Box<dyn std::error::Error>> {
    // Setup prometheus for metrics
    let prometheus = PrometheusMetricsBuilder::new("api")
        .endpoint("/metrics")
        .build()
        .unwrap();

    let openapi = match mode {
        Mode::ReadOnly => {
            // Setup OpenAPI / Swagger API infos
            #[derive(OpenApi)]
            #[openapi(
                paths(
                    read_only::get_trusted_dids,
                    read_only::get_trusted_certs,
                    read_only::check_cert
                ),
                components(
                    schemas(store::data::TrustedCerts,
                        store::data::TrustedDIDs,
                        store::data::CertFormat, 
                        store::data::Cert, 
                        store::data::CheckCertResult, 
                        store::data::CertPost,
                        store::data::DID)
                ),
                tags(
                    (name = "certs", description = "Get x509 trust information"),
                    (name = "dids", description = "Get DIDs trust information"),
                )
            )]
            struct ApiDoc;
            ApiDoc::openapi()
        }
        Mode::ReadWrite => {
            // Setup OpenAPI / Swagger API infos
            #[derive(OpenApi)]
            #[openapi(
                paths(
                    read_only::get_trusted_dids,
                    read_only::get_trusted_certs,
                    read_only::check_cert,
                    read_write::put_trusted_cert,
                    read_write::put_trusted_did
                ),
                components(
                    schemas(store::data::TrustedCerts,
                        store::data::TrustedDIDs, 
                        store::data::CertFormat, 
                        store::data::Cert, 
                        store::data::CheckCertResult, 
                        store::data::CertPost,
                        store::data::DID,
                        store::data::CertPut,
                        store::data::CertPutResult,
                        store::data::DidPutResult)
                ),
                tags(
                    (name = "certs", description = "Get x509 trust information"),
                    (name = "dids", description = "Get DIDs trust information"),
                )
            )]
            struct ApiDoc;
            ApiDoc::openapi()
        }
    };

    // Setup db store
    let mut store = LocalStore::new(store_path);
    store.open()?;

    // Setup logs
    env_logger::init_from_env(env_logger::Env::new().default_filter_or(log_level.to_string()));

    info!(
        "Get Swagger API specs on : http://{}:{}/swagger-ui/",
        &addr, &port
    );

    // Make instance variable of ApiDoc so all worker threads gets the same instance.
    let store = Data::new(store);

    let server = HttpServer::new(move || {
        let app = App::new()
            .wrap(Logger::new("%D %s %a %U %{User-Agent}i"))
            .service(
                SwaggerUi::new("/swagger-ui/{_:.*}").url("/api-doc/openapi.json", openapi.clone()),
            )
            .wrap(prometheus.clone());
        if mode == Mode::ReadWrite {
            app.configure(read_write(store.clone()))
        } else {
            app.configure(read_only(store.clone()))
        }
    });

    server.bind((addr, port))?.run().await.map_err(|e| e.into())
}

pub(super) fn read_only(store: Data<LocalStore>) -> impl FnOnce(&mut ServiceConfig) {
    |config: &mut ServiceConfig| {
        let scope = web::scope("/api/v1")            .app_data(store)
        .service(get_trusted_certs)
        .service(get_trusted_dids)
        .service(check_cert);

        config.service(scope);
    }
}

pub(super) fn read_write(store: Data<LocalStore>) -> impl FnOnce(&mut ServiceConfig) {
    |config: &mut ServiceConfig| {
        let scope = web::scope("/api/v1")
        .app_data(store)
        .service(get_trusted_certs)
        .service(get_trusted_dids)
        .service(check_cert)
        .service(put_trusted_cert)
        .service(put_trusted_did);
        
        config
            .service(scope);
    }
}


#[cfg(test)]
mod test {
    use std::fs;

    #[test]
    fn test_cert_convert() {
        let pem = fs::read("./example_certs/root-ca.pem");
        assert!(pem.is_ok());
        let decoded = x509_certificate::X509Certificate::from_pem_multiple(pem.unwrap());
        assert!(decoded.is_ok());
        decoded.unwrap().into_iter().for_each(|cert| {
            println!("Key algorithm : {:?}", &cert.key_algorithm());
            println!("Issue name : {:?}", cert.issuer_name());
            println!("Cert : {:#?}", cert);

            let der = cert.encode_der();
            assert!(der.is_ok());
            let der = der.unwrap();
            println!("DER length : {}", der.len());

            let imported = x509_certificate::X509Certificate::from_der(der);
            assert!(imported.is_ok());
            let imported = imported.unwrap();
            println!("Imported value : {:#?}", imported);
        });
    }
}
