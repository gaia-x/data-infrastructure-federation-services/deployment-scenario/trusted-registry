pub(crate) mod data;
pub(crate) mod local_store;

use crate::error::Error;

use self::data::{Cert, TrustedCerts, TrustedDIDs};

pub(crate) trait TrustedPersistence {
    fn retrieve_did_by_did(&self, did: &str) -> Result<Option<Vec<u8>>, Error>;
    fn retrieve_cert_by_pulic_key(
        &self,
        base64_encoded_key: String,
    ) -> Result<Option<Vec<u8>>, Error>;
    fn put_new_did(&self, did: String, scope: Vec<String>) -> Result<(), Error>;
    fn get_all_dids(&self) -> Result<TrustedDIDs, Error>;
    fn get_all_certs(&self) -> Result<TrustedCerts, Error>;
    fn retrieve_cert_by_dn(&self, dn: &str) -> Result<Option<Cert>, Error>;
    fn remove_did(&self, did: &str) -> Result<(), Error>;
    fn remove_cert(&self, cert: &str) -> Result<(), Error>;
    fn put_new_cert(&self, dn: String, cert: Vec<u8>, scope: Vec<String>) -> Result<(), Error>;
}
