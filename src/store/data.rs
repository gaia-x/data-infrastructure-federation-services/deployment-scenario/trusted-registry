use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

use crate::error::Error;

type Did = String;
type Base64 = String;
type DN = String;
type Binary = Vec<u8>;
type Context = String;

/// The possible certificate encoding format
#[derive(Serialize, Deserialize, Debug, Clone, ToSchema, PartialEq, Eq)]
pub(crate) enum CertFormat {
    #[serde(rename = "pem_base64")]
    /// Base64 encoded of PEM representation of the certificate.
    ///
    /// Even if the PEM format is a base64 version of the DER format, you must re-encode them in base64 before sending them to the API to avoid any \r\n char.
    /// A PEM can enclose multiples certificates (I.E. multiples BEGIN CERTIFICATE and END CERTIFICATE) in the case of certification chain.
    #[schema(example = "LS0tLS1CRUdJTiBDRVJUSU...lDQVRFLS0tLS0=", value_type = String)]
    PemBase64(Base64),
    #[serde(rename = "der_base64")]
    /// Binary encoded of DER representation of the certificate.
    #[schema(value_type = Vec<u8>)]
    Der(Binary),
}

impl CertFormat {
    pub(crate) fn to_pem(&self) -> Result<String, Error> {
        match self {
            CertFormat::PemBase64(pem) => Ok(pem.clone()),
            CertFormat::Der(der) => {
                let decoded = x509_certificate::CapturedX509Certificate::from_der(der.clone())?;
                Ok(decoded.encode_pem())
            }
        }
    }
}

/// The certificate description
#[derive(Serialize, Deserialize, Debug, Clone, ToSchema, PartialEq, Eq)]
pub(crate) struct Cert {
    /// The distinguished name of the certificate's subject
    #[schema(example = "DN : 2.5.4.6:US,2.5.4.10:Internet Security Research Group,2.5.4.3:ISRG Root X1", value_type = String)]
    pub dn: DN,
    // The enclosed format of the certificate.
    #[schema(value_type = CertFormat)]
    pub cert: CertFormat,
    /// Indicate the ontologies for which the certificate's signature is trusted
    #[schema(example = json!(["https://www.w3.org/2018/credentials/v1","https://www.gaiax.org/2022/participant/v1"]), value_type = Vec<String>)]
    pub trusted_for_context: Vec<Context>,
}

impl Cert {
    pub(crate) fn new(dn: DN, cert: CertFormat, trusted_for_context: Vec<Context>) -> Self {
        Self {
            dn,
            cert,
            trusted_for_context,
        }
    }

    pub(crate) fn to_pem_base64(&self) -> Result<Base64, Error> {
        Ok(base64::encode(&self.cert.to_pem()?))
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, ToSchema, PartialEq, Eq)]
#[allow(clippy::upper_case_acronyms)]
pub(crate) struct DID {
    /// The DID
    #[schema(example = "did:example:123", value_type = String)]
    pub did: Did,
    /// Indicate the ontologies for which the DID is trusted
    #[schema(example = json!(["https://www.w3.org/2018/credentials/v1","https://www.gaiax.org/2022/participant/v1"]), value_type = Vec<String>)]
    pub trusted_for_context: Vec<Context>,
}

#[derive(Serialize, Deserialize, Debug, Clone, ToSchema, PartialEq, Eq)]
#[allow(clippy::upper_case_acronyms)]
pub(crate) struct DidPutResult {
    /// import status
    pub status: String,
}

impl DID {
    pub(crate) fn new(did: Did, trusted_for_context: Vec<Context>) -> Self {
        Self {
            did,
            trusted_for_context,
        }
    }
}

/// A nullable list of trusted DID(s).
#[derive(Serialize, Deserialize, Debug, Clone, ToSchema, PartialEq, Eq)]
pub(crate) struct TrustedDIDs {
    /// The list of trusted DIDs
    pub trusted_dids: Vec<DID>,
}

/// A nullable list of trusted certificates(s) in PEM or DER format.
#[derive(Serialize, Deserialize, Debug, Clone, ToSchema)]
pub(crate) struct TrustedCerts {
    /// The list of trusted Certificates
    pub trusted_certs: Option<Vec<Cert>>,
}

#[derive(Serialize, Deserialize, Debug, Clone, ToSchema)]
pub struct CertPost {
    /// Base64 encoded of PEM representation of the certificate.
    ///
    /// Even if the PEM format is a base64 version of the DER format, you must re-encode them in base64 before sending them to the API to avoid any \r\n char.
    /// A PEM can enclose multiples certificates (I.E. multiples BEGIN CERTIFICATE and END CERTIFICATE) in the case of certification chain.
    #[schema(example = "LS0tLS1CRUdJTiBDRVJUSU...lDQVRFLS0tLS0=", value_type = String)]
    pub cert: String,
    /// If recursive is set to true, the certificates will be recursively checked until subject = issuer.
    pub recursive: bool,
}

#[derive(Serialize, Deserialize, Debug, Clone, ToSchema)]
pub struct CheckCertResult {
    /// Validity check result details
    pub check_result_detail: String,
    /// Validity check result
    pub check_result: bool,
}

#[derive(Serialize, Deserialize, Debug, Clone, ToSchema)]
pub struct CertPut {
    /// Base64 encoded certificate in PEM format
    pub pem_cert: String,
    /// Indicate the ontologies for which the certificate's signature is trusted
    #[schema(value_type = Vec<String>)]
    pub trusted_for_context: Vec<Context>,
}

#[derive(Serialize, Deserialize, Debug, Clone, ToSchema)]
pub struct CertPutResult {
    /// import status
    pub status: String,
}
