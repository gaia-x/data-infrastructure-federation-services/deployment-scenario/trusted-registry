pub(crate) use sled::IVec;

use crate::{error::Error, store::TrustedPersistence};

use super::data::TrustedCerts;
use super::data::{Cert, CertFormat, TrustedDIDs, DID};

const DID_TREE: &str = "did";
const CERT_TREE: &str = "cert";

pub(crate) struct LocalStore {
    store_file_path: String,
    db: Option<sled::Db>,
}

impl LocalStore {
    pub(crate) fn new(store_file_path: String) -> Self {
        Self {
            store_file_path,
            db: None,
        }
    }

    pub(crate) fn open(&mut self) -> Result<(), Error> {
        self.db = Some(sled::open(&self.store_file_path)?);
        Ok(())
    }

    pub(crate) fn delete(&mut self) -> Result<(), Error> {
        match &self.db {
            Some(db) => {
                let size = db.size_on_disk();
                match size {
                    Ok(s) => {
                        println!("delete {} bytes of data", s);
                        Ok(std::fs::remove_dir_all(&self.store_file_path)?)
                    }
                    Err(_) => Err(Error::DbSize),
                }
            }
            None => Err(Error::NoOpenStore),
        }
    }
}

impl TrustedPersistence for LocalStore {
    fn retrieve_did_by_did(&self, did: &str) -> Result<Option<Vec<u8>>, Error> {
        match &self.db {
            Some(db) => {
                let tree = db.open_tree(DID_TREE)?;
                let res = tree.get(did)?;
                Ok(res.map(|r| r.as_ref().into()))
            }
            None => Err(Error::NoOpenStore),
        }
    }

    fn retrieve_cert_by_pulic_key(
        &self,
        base64_encoded_key: String,
    ) -> Result<Option<Vec<u8>>, Error> {
        match &self.db {
            Some(db) => {
                let tree = db.open_tree(CERT_TREE)?;
                let res = tree.get(base64_encoded_key)?;
                Ok(res.map(|r| r.as_ref().into()))
            }
            None => Err(Error::NoOpenStore),
        }
    }

    fn put_new_did(&self, did: String, scope: Vec<String>) -> Result<(), Error> {
        match &self.db {
            Some(db) => {
                let tree = db.open_tree(DID_TREE)?;
                let did_obj = DID::new(did.clone(), scope);
                let encoded_did = bincode::serialize(&did_obj)?;
                tree.insert(did, encoded_did).map(|_| Ok(()))?
            }
            None => Err(Error::NoOpenStore),
        }
    }

    fn put_new_cert(&self, dn: String, cert: Vec<u8>, scope: Vec<String>) -> Result<(), Error> {
        match &self.db {
            Some(db) => {
                let tree = db.open_tree(CERT_TREE)?;
                let cert_obj = Cert::new(dn.clone(), CertFormat::Der(cert), scope);
                let encoded_cert = bincode::serialize(&cert_obj)?;
                tree.insert(dn, encoded_cert).map(|_| Ok(()))?
            }
            None => Err(Error::NoOpenStore),
        }
    }

    fn get_all_dids(&self) -> Result<TrustedDIDs, Error> {
        match &self.db {
            Some(db) => {
                let tree = db.open_tree(DID_TREE)?;
                let r = tree.scan_prefix("");

                let res = TrustedDIDs {
                    trusted_dids: r
                        .collect::<Result<Vec<(IVec, IVec)>, sled::Error>>()?
                        .into_iter()
                        .map(|(k, v)| -> Result<DID, Error> {
                            let did = std::str::from_utf8(&k).map(|str| str.to_string());
                            let full_object = bincode::deserialize::<DID>(&v);
                            match (did, full_object) {
                                (Ok(_), Ok(full_object)) => Ok(full_object),
                                (Err(e), Ok(_)) => Err(Error::DIDParseError(e.to_string())),
                                (Ok(_), Err(e)) => Err(Error::DIDParseError(e.to_string())),
                                (Err(e1), Err(e2)) => Err(Error::DIDParseError(format!(
                                    "DID parse error: {:?}, trusted_for_context parse error: {:?}",
                                    e1, e2
                                ))),
                            }
                        })
                        .collect::<Result<Vec<DID>, Error>>()?,
                };
                Ok(res)
            }
            _ => Err(Error::NoOpenStore),
        }
    }

    fn remove_did(&self, did: &str) -> Result<(), Error> {
        match &self.db {
            Some(db) => {
                let tree = db.open_tree(DID_TREE)?;
                tree.remove(did)?;
                Ok(())
            }
            _ => Err(Error::NoOpenStore),
        }
    }

    fn remove_cert(&self, cert: &str) -> Result<(), Error> {
        match &self.db {
            Some(db) => {
                let tree = db.open_tree(CERT_TREE)?;
                tree.remove(cert)?;
                Ok(())
            }
            _ => Err(Error::NoOpenStore),
        }
    }

    fn get_all_certs(&self) -> Result<TrustedCerts, Error> {
        match &self.db {
            Some(db) => {
                let tree = db.open_tree(CERT_TREE)?;
                let r = tree.scan_prefix("");

                let extract = r
                    .map(|r| -> Result<(String, Cert), Error> {
                        match r {
                            Ok((k, v)) => {
                                let dn = std::str::from_utf8(&k)?;
                                let cert = bincode::deserialize::<Cert>(&v)?;
                                let pem_base64 = cert.to_pem_base64()?;
                                Ok((
                                    dn.to_string(),
                                    Cert::new(
                                        dn.to_string(),
                                        CertFormat::PemBase64(pem_base64),
                                        cert.trusted_for_context,
                                    ),
                                ))
                            }
                            Err(e) => Err(e.into()),
                        }
                    })
                    .collect::<Result<Vec<(String, Cert)>, Error>>()?;
                let result = match extract.is_empty() {
                    true => None,
                    false => Some({
                        let c: Vec<Cert> = extract.into_iter().map(|(_, cert)| cert).collect();
                        c
                    }),
                };
                Ok(TrustedCerts {
                    trusted_certs: result,
                })
            }
            _ => Err(Error::NoOpenStore),
        }
    }

    fn retrieve_cert_by_dn(&self, dn: &str) -> Result<Option<Cert>, Error> {
        match &self.db {
            Some(db) => {
                let tree = db.open_tree(CERT_TREE)?;
                let res = tree.get(dn)?;
                match res {
                    Some(found) => Ok(Some(bincode::deserialize::<Cert>(&found)?)),
                    None => Ok(None),
                }
            }
            None => Err(Error::NoOpenStore),
        }
    }
}

#[cfg(test)]
mod test {
    use super::LocalStore;
    use crate::store::{
        data::{TrustedDIDs, DID},
        TrustedPersistence,
    };

    #[test]
    fn test_open() {
        const BASE_PATH: &str = "test.db";
        let mut ls = LocalStore::new(BASE_PATH.into());
        let open = ls.open();
        assert!(open.is_ok());
        let delete = ls.delete();
        assert!(delete.is_ok())
    }

    #[test]
    fn test_write_did_doc() {
        const BASE_PATH: &str = "test_write_did_doc.db";
        let mut ls = LocalStore::new(BASE_PATH.into());
        let open = ls.open();
        println!("{open:?}");
        assert!(open.is_ok());
        let did = "did:web:test.com".to_owned();
        let scope = vec![
            "https://www.w3.org/2018/credentials/v1".to_owned(),
            "https://www.gaiax.org/2021/participant/v1".to_owned(),
        ];
        let put = ls.put_new_did(did.clone(), scope);
        assert!(put.is_ok());
        let get = ls.retrieve_did_by_did(&did);
        assert!(get.is_ok());
        println!("DID Doc for {} : {:?}", did, get);
        let delete = ls.delete();
        assert!(delete.is_ok())
    }

    #[test]
    fn test_store_not_open_errors() {
        const BASE_PATH: &str = "test_errors.db";
        let ls = LocalStore::new(BASE_PATH.into());

        // we don't open the DB
        let did = "did:web:test.com".to_owned();
        let scope = vec![
            "https://www.w3.org/2018/credentials/v1".to_owned(),
            "https://www.gaiax.org/2021/participant/v1".to_owned(),
        ];
        let put = ls.put_new_did(did, scope);
        assert!(put.is_err());
        println!("{:?}", put.unwrap_err().to_string());
    }

    #[test]
    fn test_get_dids() {
        const BASE_PATH: &str = "test_get_dids.db";
        let mut ls = LocalStore::new(BASE_PATH.into());
        let open = ls.open();
        assert!(open.is_ok());
        let did = "did:web:test.com".to_owned();
        let scope = vec![
            "https://www.w3.org/2018/credentials/v1".to_owned(),
            "https://www.gaiax.org/2021/participant/v1".to_owned(),
        ];
        let put = ls.put_new_did(did, scope.clone());
        assert!(put.is_ok());
        let get = ls.get_all_dids();
        println!("trusted DIDs : {:#?}", get);
        assert!(get.is_ok());
        assert!(
            get.unwrap()
                == TrustedDIDs {
                    trusted_dids: vec![DID {
                        did: "did:web:test.com".to_owned(),
                        trusted_for_context: scope
                    }]
                }
        );
        println!("{:#?}", ls.get_all_dids());
        let delete = ls.delete();
        assert!(delete.is_ok())
    }
}
