use x509_certificate::{rfc3280::Name, CapturedX509Certificate};

use crate::error::Error;

pub(crate) fn check_cert(cert: &str, other: &str) -> Result<(), Error> {
    let to_verify = x509_certificate::CapturedX509Certificate::from_pem(cert.as_bytes())?;
    let signer = x509_certificate::CapturedX509Certificate::from_pem(other.as_bytes())?;

    to_verify
        .verify_signed_by_certificate(signer)
        .map_err(|e| e.into())
}

pub trait CertUtil {
    fn get_issuer_dn(&self) -> Result<String, Error>;
    fn get_dn(&self) -> Result<String, Error>;
}

impl CertUtil for CapturedX509Certificate {
    fn get_issuer_dn(&self) -> Result<String, Error> {
        to_string(self.issuer_name())
    }

    fn get_dn(&self) -> Result<String, Error> {
        to_string(self.subject_name())
    }
}

fn to_string(name: &Name) -> Result<String, Error> {
    name.iter_attributes()
        .map(|attribute| {
            let oid = attribute.typ.to_string();
            attribute
                .value
                .to_string()
                .map_err(|e| e.into())
                .map(|value| format!("{}:{}", oid, value))
        })
        .collect::<Result<Vec<String>, Error>>()
        .map(|v| v.join(","))
}

#[cfg(test)]
mod test {
    use super::check_cert;
    use crate::cert::CertUtil;

    #[test]
    fn test_check_cert_with_intermediate() {
        let intermediate = include_str!("../../example_certs/intermediate.pem");
        let rootca = include_str!("../../example_certs/root-ca.pem");
        let res = check_cert(intermediate, rootca);
        assert!(res.is_ok());
        println!("Result : {:?}", res);
    }

    #[test]
    fn test_get_dn() {
        let solutionsim = include_str!("../../example_certs/root-ca.pem");
        let cert = x509_certificate::CapturedX509Certificate::from_pem(solutionsim);
        assert!(cert.is_ok());
        let dn = cert.unwrap().get_dn();
        assert!(dn.is_ok());
        println!("DN : {}", dn.unwrap());
    }

    #[test]
    fn test_issuer_dn() {
        let solutionsim = include_str!("../../example_certs/root-ca.pem");
        let cert = x509_certificate::CapturedX509Certificate::from_pem(solutionsim);
        assert!(cert.is_ok());
        let dn = cert.unwrap().get_issuer_dn();
        assert!(dn.is_ok());
        println!("Issuer : {}", dn.unwrap());
    }

    #[test]
    fn test_eidas_certs() {
        let eidas = include_str!("../../example_certs/certs_eidas.pem");
        let cert = x509_certificate::CapturedX509Certificate::from_pem_multiple(eidas);
        assert!(cert.is_ok());
        let cert = cert.unwrap();
        println!("EIDAS : {:?} certs", cert.len());
        cert.iter().for_each(|c| {
            let dn = c.get_dn();
            println!("DN : {:?}", dn);
            let issuer = c.get_issuer_dn();
            println!("Issuer : {:?}", issuer);
        });
    }
}
