use std::fs;

use clap::Parser;
use methods::{
    cli::{add_cert, add_did, delete_db, remove_cert, remove_did},
    serve,
};
use store::data::CertFormat;

pub(crate) mod cert;
mod config;
mod error;
mod methods;
pub(crate) mod store;

#[cfg(not(target_env = "msvc"))]
use tikv_jemallocator::Jemalloc;

#[cfg(not(target_env = "msvc"))]
#[global_allocator]
static GLOBAL: Jemalloc = Jemalloc;

#[actix_web::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = config::Args::parse();
    match args.command {
        config::Operation::Serve { log_level, mode } => {
            serve(
                args.listen_ip,
                args.listen_port,
                args.db_path,
                mode,
                log_level,
            )
            .await
        }
        config::Operation::AddPEMCert { file_path, scope } => {
            let pem_content = fs::read_to_string(file_path)?;
            add_cert(args.db_path, CertFormat::PemBase64(pem_content), scope).await
        }
        config::Operation::RemovePEMCert { base_64_pem_cert } => {
            remove_cert(args.db_path, base_64_pem_cert).await
        }
        config::Operation::AddDID { did, scope } => add_did(args.db_path, did, scope).await,
        config::Operation::RemoveDID { did } => remove_did(args.db_path, did).await,
        config::Operation::DeleteDB { confirm } => delete_db(args.db_path, confirm).await,
        config::Operation::AddDERCert { file_path, scope } => {
            let der_content = fs::read(file_path)?;
            add_cert(args.db_path, CertFormat::Der(der_content), scope).await
        }
    }
}
